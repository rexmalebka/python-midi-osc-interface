from pythonosc import osc_message_builder
from pythonosc import udp_client
import rtmidi
from json import loads
from re import match

class axiom():
    """
    This class contains the necessary to improve midi interface to OSC 
    I tested it with my M-Audio Axiom Air mini 32

    my midi controller keyboard has:
        -32 velocity sensible keys
        -16 velocity sensible pads
        -record, stop and play buttons
        -8 knots
        -sustain button
        -3 wheels <- ^ ->
        -5 direction buttons

    The midi controller keyboard could be clustered in groups:
        -keys events
        -pads events
        -controller events
        -wheels events

    I wanted to joined midi and OSC in the same interface, adding actions to keys/pads and triggering
    functions, for any of the keys/pads triggered or individual key/pad triggered without checking
    it manually


    """

    def __init__(self,configuration):
        """
        configuration format will be released later
        """

        #initialize OSC
        if 'OSC' in configuration:
            osc_config = configuration['OSC']
            self.OSCClient = udp_client.SimpleUDPClient(osc_config['host'],osc_config['port'])
        else:
            self.OSCClient = ""

        #initialize keys
        if 'keys' in configuration:
            key_config = configuration['keys']
            
            #add keys to properties main object
            self.keys = self.Keys(
                    keyrange = key_config['range'],
                    start = key_config['start'],
                    OSCAddress = key_config['OSCAddress']
                    )

        #initialize pads
        if 'pads' in configuration:
            pad_config = configuration['pads']
            self.pads = self.Pads(
                    padrange = pad_config['range'],
                    OSCAddress = pad_config['OSCAddress']
                    )

            self.pads.OSCClient = self.OSCClient
        if 'controllers' in configuration:
            controller_config = configuration['controllers']
            self.controllers = self.Controllers(controller_config)

        #initialize midi
        if 'midi' in  configuration: 
            midi_config = configuration['midi']
            self.midi_rtmidi = rtmidi.RtMidiIn()
            self.midi_timeout = midi_config['timeout']
            self.midi_port = midi_config['port']
            self.midi_rtmidi.openPort(self.midi_port)

    class Controllers():
        def __init__(self,controller_config):
            for control in controller_config:
                setattr(
                        self,
                        control,
                        self.Controller(
                            name = control,
                            OSCAddress = controller_config[control]['OSCAddress'],
                            number = controller_config[control]['number'],
                            )
                        )
                #self.controllers = self.

        class Controller():
            def __init__(self, name, OSCAddress, number):
                self.name = name
                self.OSCAddress = OSCAddress
                self.number = number
                self.value = 0


    class Wheels():
        """
        class for wheel object generating
        """
        class Wheel():
            pass
        pass

    class Pads():
        """
        class for pad object generating
        """

        def __init__(self, padrange = (36,51), OSCAddress = "/sample"):
            """
            Adds attribute to the object acording to pad name
            """
            self.range = padrange
            self.OSCAddress = OSCAddress
            for n,k in enumerate(range(self.range[0],self.range[1]+1)):
                setattr(
                        self,
                        "pad{}".format(n),
                        self.Pad(
                            name = "pad{}".format(n),
                            number = k
                            )
                        )

        def TriggerFunction(self, name, number, velocity, state):
            """
            executes a function, 
            """
            return (name,velocity, state)


        def OSCFunction(self, client, name,number, velocity, state ):
            client.send_message(self.OSCAddress,['number',number,'name',name,'velocity',velocity,'state',state])
            return (name,number,velocity,state)

        def getpad(self,k):
            """
            this function return the name of the object that matches the number of the midi message
            """
            name = 'pad'+str(k - self.range[0])
            return name

        class Pad():
            """
            pad class
            """

            def __init__(self, name  , number ):
                self.name = name
                self.number = number
                self.velocity = 0
                self.OSCFunction = self.__OSCFunction
                self.TriggerFunction = self.__TriggerFunction
                self.OSCAddress = ""

            def __TriggerFunction(self):
                """
                executes a function
                """
                return (True)

            def __OSCFunction(self, client):
                """
                executes a function
                """
                if (self.OSCAddress != ""):
                    client.send_message(self.OSCAddress,['number',self.number,'name',self.name,'velocity',self.velocity,'state',self.state])
                return (True)


    class Keys():
        """
        class for key object generating
        """

        __prim = ['C','Cs','D','Ds','E','F','Fs','G','Gs','A','As','B']
        __notepattern = r"^([CDFGA][s]?[0-9]+$|[EB][0-9]+)$"

        def __init__(self, keyrange = (0,32), start = 'C0', OSCAddress = "\\piano"):

            self.range = keyrange
            self.OSCAddress = OSCAddress

            #check that start chord match the pattern and resambles the __prim list
            if match(self.__notepattern, start):
                if 's' in start:
                    number = start.split('s')[0]+'s'
                    scale = int(start.split('s')[-1])
                else:
                    number = start[0]
                    scale = int(start[1:])

                self.__prim = self.__prim[self.__prim.index(number):] + self.__prim[:self.__prim.index(number)]

            #iterates over the range
            for k in range(self.range[0],self.range[1]+1):
                setattr(
                        self,
                        '{}{}'.format(self.__prim[k%12],int(( k - k%12) / 12 ) + scale),
                        self.Key(
                            name = '{}{}'.format(self.__prim[k%12],int((k - k % 12) / 12)),
                            number = number
                            )
                        )
        def TriggerFunction(self, name, number, velocity, state):
            """
            executes a function, 
            """
            print(name,number,velocity,state)
            return (name, number, velocity, state)

        def OSCFunction(self, client, name, number, velocity, state ):

            client.send_message(self.OSCAddress,['number',number,'name',name,'velocity',velocity,'state',state])
            return (name,number,velocity,state)

        def getkey(self,k):
            """
            this function return the name of the object that matches the number of the midi message
            """
            name = '{}{}'.format(self.__prim[k%12],int( ( k - k % 12 ) / 12 ))
            return name

        class Key():
            def __init__(self, name,  number):
                self.name = name
                self.number = number
                self.velocity = 0
                self.state = 'off'
                self.TriggerFunction = self.__TriggerFunction
                self.OSCFunction = self.__OSCFunction
                self.OSCAddress = ""


            def __TriggerFunction(self):
                """
                executes a function
                """
                return (True)

            def __OSCFunction(self, client):
                """
                executes a function
                """
                if self.OSCAddress != "":
                    client.send_message(self.OSCAddress,['number',self.number,'velocity',self.velocity,'state',self.state,'name',self.name])
                return ()

    def loop(self):
        while True:
            self.message = self.midi_rtmidi.getMessage(self.midi_timeout)

            if type(self.message) != type(None):

                #is a note or a controller?
                if self.message.isNoteOnOrOff():

                    #is a key or a pad
                    number = self.message.getControllerNumber()
                    krange = self.keys.range
                    prange = self.pads.range

                    if ( number >= krange[0]) and ( number <= krange[1]):

                        name = self.keys.getkey( number )
                        velocity = self.message.getVelocity() 

                        if  self.message.isNoteOn():
                            state = 'on'
                        else:
                            state = 'off'

                        self.keys.__dict__[name].velocity = velocity
                        self.keys.__dict__[name].state = state

                        self.keys.TriggerFunction(
                                name = name,
                                number = number,
                                velocity = velocity,
                                state = state
                                )

                        self.keys.OSCFunction(
                                client = self.OSCClient,
                                name = name,
                                number = number,
                                velocity = velocity,
                                state = state
                                )

                        self.keys.__dict__[name].TriggerFunction()
                        self.keys.__dict__[name].OSCFunction(client = self.OSCClient)


                    if (number >= prange[0]) and ( number <= prange[1]):

                        name = self.pads.getpad( number )
                        velocity = self.message.getVelocity() 

                        if  self.message.isNoteOn():
                            state = 'on'
                        else:
                            state = 'off'

                        velocity = self.message.getVelocity() 

                        self.pads.__dict__[name].velocity = velocity
                        self.pads.__dict__[name].state = state

                        self.pads.TriggerFunction(
                                name = name,
                                number = number,
                                velocity = velocity,
                                state = state
                                )

                        self.pads.OSCFunction(
                                client = self.OSCClient,
                                name = name,
                                number = number,
                                velocity = velocity,
                                state = state
                                )

                        self.pads.__dict__[name].TriggerFunction()
                        self.pads.__dict__[name].OSCFunction(client = self.OSCClient)




with open('configuration.conf') as conf:
    configuration = conf.read()

configuration = loads(configuration.replace("'",'"'))

a = axiom(configuration['configuration'])
cliente = a.OSCClient

#a.keys.Cs0.OSCAddress="/SATAAAAN"


a.loop()
