# Python midi-OSC interface

## What does this do ?

I wanted to make an interface for my midi controller keyboard, I used my M-Audio Axiom air mini 32 controller as example, but I hope in the future to generalised for more controllers

![m-audio axiom air mini 32](http://kb.inmusicbrands.com/media/images/maudio/AxiomAIRMini32.png "m-audio axiom air mini 32 ")

I use my midi controller in Supercollider, but it wasn't so convenient to programm it in there, so I'm making this thinking in supercollider, I wanted python to make the midi job and supercollider to do the sound stuff only, I use OSC to communicate python - supercollider.

## What does midi controller have ? 

* keys for triggering notes
* Pads for triggering samples
* Knots for adjusting values
* Buttons for miscellaneous actions
* Wheels for adjusting values

Boths keys and samples have a number assigned, depending on the model those could change, keys and samples sometimes are velocity sensible.

## midi control

keys and pads could trigger functions, Sending OSC and executing funcitions

## configuration

we need to configurate it before using it, I use dictionaries.

### **keys**

* **range** : (_start_,_end_)  defines a range from int _start_ to int _end_ , those will be the values that will match from the midi object number.
* **start** : "_starting chord_"  defines a str where to start counting the note, altered notes are only sharp, replacing sharp symbol "\#" for "s", scale is positive, regex format: `[CDFGA][s]?[0-9]+|[EB][0-9]+`  example: "Cs0" "D2" "Fs1"
* **OSCAddres** : "_address_" defines a OSC Address 

### **pads**

* **range** : (_start_, _end_)  defines a range from int _start_ to int _end_ , those will be the values that will match from the midi object number.
* **OSCAddres** : "_address_" defines a OSC Address 

### **controllers**

* **"_custom name_"** :
  * **number** : int value of the controller to match to
* **OSCAddres** : "_address_" defines a OSC Address 

### **midi**

* **timeout** : _midi timeout_  defines the int timeout for reading midi messages
* **port** : _midi port_ defines the int midi port that will be open for reading

### **OSC**

* **host** : "_host_" defines the host of OSC server
* **port** : _port_ defines an int port of the OSC server

example:
```
        'keys':{
            'range' : (0,31),
            'start' : 'C0',
            'OSCAddres' : '/piano'
            },
        'pads':{
            'range' : (36,51),
            'OSCAddres' : '/sample'
            },
        'midi':{
            'port' : 1,
            'timeout': 250
            },
        'OSC':{
            'host':'127.0.0.1',
            'port': 57120
            }
        }
```

## Attributes and methods

When creating an object from the main class, the class would create diferent objects acording to the configuration and the existance of keys, pads, controllers and wheels

Example:

32 keys(0-31) from C0 , 16 pads (36-52) no controllers, no wheels
/piano is the OSCAddress for keys , /sample is the address for pads
```
a = axiom(configuration)

a.Keys(      a.OSCClient  a.Pads(      a.Wheels(    a.keys       a.loop(      a.pads       a.Controllers(
```

* **Keys, Pads, Wheels, Controllers** Classes
* **keys, pads** objects generated
* **loop** main loop function
* **OSCClient** OSC client object

```
a.keys.A0                a.keys.C0                a.keys.D1                a.keys.E1                a.keys.Fs1               a.keys.Gs1               a.keys.getkey(
a.keys.A1                a.keys.C1                a.keys.D2                a.keys.E2                a.keys.Fs2               a.keys.Gs2               a.keys.range
a.keys.As0               a.keys.Cs0               a.keys.Ds0               a.keys.F0                a.keys.G0                a.keys.Key(              
a.keys.As1               a.keys.Cs1               a.keys.Ds1               a.keys.F1                a.keys.G1                a.keys.OSCAddress        
a.keys.B0                a.keys.Cs2               a.keys.Ds2               a.keys.F2                a.keys.G2                a.keys.OSCFunction(      
a.keys.B1                a.keys.D0                a.keys.E0                a.keys.Fs0               a.keys.Gs0               a.keys.TriggerFunction(  
```

* objects made from the range of keys
* **OSCAddress** for OSC Comunicatoin
* **OSCFunction** for sending OSC Messages
* 

```
a.pads.OSCAddress        a.pads.TriggerFunction(  a.pads.pad10             a.pads.pad13             a.pads.pad2              a.pads.pad5              a.pads.pad8
a.pads.OSCFunction(      a.pads.pad0              a.pads.pad11             a.pads.pad14             a.pads.pad3              a.pads.pad6              a.pads.pad9
a.pads.Pad(              a.pads.pad1              a.pads.pad12             a.pads.pad15             a.pads.pad4              a.pads.pad7              a.pads.range
```
